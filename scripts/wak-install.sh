#!/bin/sh

# Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>
#
# This program is free software, you can redistribute it and/or
# modify it under the terms of the MIT/X11 license.
#
# You should have received a copy of the MIT/X11 license along with
# this program. If not, see
# <http://www.opensource.org/licenses/mit-license.php>.

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: wak-install.sh PACKAGE-DIRECTORY TARGET-DIRECTORY"
    echo
    echo "Installs the Wak package in PACKAGE-DIRECTORY into TARGET-DIRECTORY."
    echo
    exit 1
fi

pkg_dir="$1"
dest="$2"

set -e

mkdir -p "$dest"

( cd "$pkg_dir" && find . -name "*.sls" \
    -o \( -name "*.scm" -a ! -path "*/tests/*" \
    -a ! -name pkg-list.scm \) \
    | tar -c -T - ) | tar -C "$dest" -x
